package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;

import java.util.List;
import java.util.stream.Stream;

public interface GameState
{
	GamePlan getGamePlan();

	boolean isGameOver();
	int getCurrentRound();
	int getRoundsRemaining();

	List<Player> getPlayers();

	List<Ant> getAntsOfPlayer(int playerId);
	int getPlayerResources(int playerId);
	void setPlayerResources(int playerId, int resources);

	Ant spawnAnt(Point position, int playerId);
	void killAnt(Ant ant);
	void moveAnt(Ant ant, Point targetPosition);

	FieldState getField(Point position);
	Stream<FieldPos> allFields();
	void setResourcesOnField(Point position, int resources);

    GameStateDTO toDto();

    class FieldPos
	{
		public final Point position;
		public final FieldState fieldState;

		public FieldPos(Point position, FieldState fieldState)
		{
			this.position = position;
			this.fieldState = fieldState;
		}
	}
}
