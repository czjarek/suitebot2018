package suitebot3.game;

public interface FieldState
{
    /** Get number of resources available on the field */
    int getResourceCount();

    /** Get an ant standing on the field or null. */
    Ant getAnt();

    /** Get owner a home base located on the field or null. */
    Player getBase();
}
