package suitebot3.game;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

public class PlayerState
{
	private List<UpdatableAnt> ants;
	private final Player player;
	private int resources;

	public PlayerState(Player player)
	{
		this.player = player;
		ants = new ArrayList<>(10);
		resources = GameConstants.PLAYER_INITIAL_RESOURCES;
	}

	public Player getPlayer()
	{
		return player;
	}

	public List<Ant> getAnts()
	{
		return ImmutableList.copyOf(ants);
	}

	public UpdatableAnt getAnt(int i)
	{
		return ants.get(i);
	}

	public UpdatableAnt spawnNewAnt(Point position)
	{
		UpdatableAnt newAnt = new PlayersAnt(position, player);
		ants.add(newAnt);
		return newAnt;
	}

	public void killAnt(Ant ant)
	{
		ants.remove(ant);
	}

	public int getResources()
	{
		return resources;
	}

	public void setResources(int resources)
	{
		this.resources = resources;
	}

	private class PlayersAnt implements UpdatableAnt
	{
		private Point position;
		private final Player owner;
		private Action lastMove;

		private PlayersAnt(Point position, Player owner)
		{
			this.position = position;
			this.owner = owner;
		}

		@Override
		public void setPosition(Point position)
		{
			this.position = position;
		}

		@Override
		public Point getPosition()
		{
			return position;
		}

		@Override
		public Player getPlayer()
		{
			return owner;
		}

		@Override
		public void kill()
		{
			killAnt(this);
		}

		@Override
		public Action getLastMove()
		{
			return lastMove;
		}

		@Override
		public void setLastMove(Action lastMove)
		{
			this.lastMove = lastMove;
		}
	}
}
