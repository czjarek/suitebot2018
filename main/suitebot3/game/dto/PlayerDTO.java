package suitebot3.game.dto;

import suitebot3.game.Action;
import suitebot3.game.Point;

import java.util.List;

/**
 * Do not modify.
 */
public class PlayerDTO
{
    public String name;
    public int resources;
    public Point homeBaseLocation;
    public List<Point> antLocations;
    public List<Action> antLastMoves;
}
