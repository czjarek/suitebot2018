package suitebot3.game;

public interface Ant
{
    /** The owner of the ant */
    Player getPlayer();

    /** Current position of the ant */
    Point getPosition();

    /** Returns the last action the ant did or null. */
    Action getLastMove();
}
