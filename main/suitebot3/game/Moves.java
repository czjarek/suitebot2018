package suitebot3.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Class to record the moves your ants should do.
 * Use inner class Moves.Builder to create an instance easily.
 */
public class Moves
{
	private final List<Action> antActions;
	private final boolean spawnNewAnt;

	public Moves(Action... actions)
	{
		this(false, actions);
	}

	public Moves(boolean spawnNewAnt, Action... actions)
	{
		this(spawnNewAnt, actions == null ? Collections.singletonList(null) : Arrays.asList(actions));
	}

	public Moves(boolean spawnNewAnt, List<Action> antActions)
	{
		this.antActions = new ArrayList<>(antActions);
		this.spawnNewAnt = spawnNewAnt;
	}

	public String serialize()
	{
		return toString();
	}

	public Action getAction(int ant)
	{
		return antActions.get(ant);
	}

	public int getDirectionSize()
	{
		return antActions.size();
	}

	public boolean getSpawnNewAnt()
	{
		return spawnNewAnt;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Moves moves = (Moves) o;

		if (spawnNewAnt != moves.spawnNewAnt) return false;
		return antActions.equals(moves.antActions);
	}

	@Override
	public int hashCode()
	{
		int result = antActions.hashCode();
		result = 31 * result + (spawnNewAnt ? 1 : 0);
		return result;
	}

	@Override
	public String toString()
	{
		String string = "";
		for (Action action : antActions)
		{
			if (action == null)
				string += Action.HOLD;
			else
				string += action;
		}

		if (spawnNewAnt)
			string += "S";

		return string;
	}

	public static class Builder
	{
		private final List<Action> antActions;
		private boolean spawnNewAnt = false;
		private final List<Ant> myAnts;

		public Builder(GameState gameState, int playerId)
		{
			this.myAnts = gameState.getAntsOfPlayer(playerId);

			antActions = new ArrayList<>(myAnts.size());
			for (int i = 0; i < myAnts.size(); ++i)
				antActions.add(null);
		}

		public Builder moveAnt(Ant ant, Action action)
		{
			int i = myAnts.indexOf(ant);
			if (i < 0)
				throw new RuntimeException("Ant " + ant + " doesn't belong to the player");

			antActions.set(i, action);
			return this;
		}

		public Builder spawnNewAnt()
		{
			spawnNewAnt = true;
			return this;
		}

		public Moves build()
		{
			return new Moves(spawnNewAnt, antActions);
		}

	}
}
