package suitebot3.game;

import java.util.Map;

public interface GamePlan
{
	int getWidth();
	int getHeight();

	/** Throws PointOutsideOfGamePlan exception if the position is not withing dimensions of the game plan. */
	void checkIsOnPlan(Point position);

	/** Map of home base positions. The key to the map is the player id. */
	Map<Integer, Point> getHomeBasePositions();

	/** Maximum number of rounds in a game. */
	int getMaxRounds();

	class PointOutsideOfGamePlan extends RuntimeException
	{
		public PointOutsideOfGamePlan(Point point)
		{
			super("Point " + point + " is outside of game plan");
		}
	}
}
