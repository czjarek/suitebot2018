package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;
import suitebot3.game.dto.PlayerDTO;
import suitebot3.game.util.VisualisationUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GameStateCreator
{

	/**
	 * Creates a game state from a string representation.
	 * See VisualisationUtil.java for description of all available characters.
	 * In general:
	 *  - Capital letters A-H are locations of home bases. Ants are placed on the home bases.
	 *  - Small letters a-h represent ants.
	 *  - Characters " .:-=+*#%@" are fields with different number of resources on it.
	 *    ' '  has 0 resources,
	 *    '.' a little bit
	 *    ...
	 *    '@' has maximum number of resources.
	 *
	 * For example the following will create 5x5 game plan.
	 * There will be:
	 *  - 2 ants of player 1 (letters 'A' and 'a')
	 *  - one ant of player 2 in the home base 'B'
	 *  - little bit of resources in the upper left corner
	 *  - lot of resources along the bottom line.
	 *
	 *     "...a \n" +
	 *     ".A   \n" +
	 *     "     \n" +
	 *     "  B  \n" +
	 *     "#####\n" +
	 */
	public static StandardGameState fromString(String gameStateAsString)
	{
		List<Player> players = new ArrayList<>();
		Map<Integer, Point> startingPositions = new HashMap<>();

		String[] lines = gameStateAsString.replaceAll("\n$", "").split("\n");
		assertRectangularPlan(lines);

		int width = lines[0].length();
		int height = lines.length;

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				char ch = lines[y].charAt(x);

				if (VisualisationUtil.isBaseChar(ch))
				{
					int playerId = VisualisationUtil.getPlayerId(ch);
					players.add(new Player(playerId, "Player " + playerId));
					startingPositions.put(playerId, new Point(x, y));
				}
				else if (!VisualisationUtil.isAntChar(ch) && !VisualisationUtil.isResourceChar(ch))
					throw new GameStateCreationException("unrecognized character: " + ch);
			}
		}

		StandardGamePlan gamePlan = new StandardGamePlan(width, height, startingPositions);
		StandardGameState gameState = new StandardGameState(gamePlan, players, true);

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				char ch = lines[y].charAt(x);
				Point position = new Point(x, y);

				if (VisualisationUtil.isAntChar(ch))
				{
					gameState.spawnAnt(position, VisualisationUtil.getPlayerId(ch));

					gameState.setResourcesOnField(position, 0);
				}
				else if (VisualisationUtil.isResourceChar(ch))
					gameState.setResourcesOnField(position, VisualisationUtil.getResources(ch));
				else if (!VisualisationUtil.isBaseChar(ch))
					throw new GameStateCreationException("unrecognized character: " + ch);
			}
		}

		return gameState;
	}

	private static void assertRectangularPlan(String[] lines)
	{
		int width = lines[0].length();

		for (int i = 1; i < lines.length; i++)
		{
			if (lines[i].length() != width)
			{
				throw new GameStateCreationException(
						String.format("non-rectangular plan: line %d width (%d) is different from the line 1 width (%d)",
								(i + 1), lines[i].length(), width));
			}
		}
	}

	public static StandardGameState fromDto(GameStateDTO dto)
	{
		List<Player> players = createPlayerListFromDto(dto);
		GamePlan gamePlan = createGamePlanFromDto(dto);

		StandardGameState gameState = new StandardGameState(gamePlan, players, dto.currentRound, dto.remainingRounds);

		for (Map.Entry<Integer, PlayerDTO> player : dto.players.entrySet())
		{
			PlayerDTO playerDTO = player.getValue();

			if (playerDTO.antLocations.size() != playerDTO.antLastMoves.size())
				throw new RuntimeException("Array of antLocations has different length than antLastMoves");

			for (int i = 0; i < playerDTO.antLocations.size(); ++i)
			{
				Ant ant = gameState.spawnAnt(playerDTO.antLocations.get(i), player.getKey());
				((UpdatableAnt)ant).setLastMove(playerDTO.antLastMoves.get(i));
			}
		}

		for (int x = 0; x < dto.gamePlanWidth; x++)
		{
			for (int y = 0; y < dto.gamePlanHeight; y++)
				gameState.setResourcesOnField(new Point(x, y), dto.fieldResources.get(x).get(y));
		}

		for (Map.Entry<Integer, PlayerDTO> player : dto.players.entrySet())
			gameState.setPlayerResources(player.getKey(), player.getValue().resources);

		return gameState;
	}

	public static GamePlan createGamePlanFromDto(GameStateDTO dto)
	{
		Map<Integer, Point> startingPositions = dto.players.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().homeBaseLocation));

		return new StandardGamePlan(dto.gamePlanWidth, dto.gamePlanHeight, startingPositions, dto.remainingRounds + dto.currentRound - 1);
	}

	public static List<Player> createPlayerListFromDto(GameStateDTO dto)
	{
		return dto.players.keySet().stream()
				.map(playerId -> new Player(playerId, dto.players.get(playerId).name))
				.collect(Collectors.toList());
	}

	public static class GameStateCreationException extends RuntimeException
	{
		public GameStateCreationException(String message)
		{
			super(message);
		}
	}
}
