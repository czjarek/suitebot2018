package suitebot3.game;

public class GameConstants
{
    public static final int PLAYER_INITIAL_RESOURCES = 100;
    public static final int ANT_CONSUMPTION_RATE = 10;

    public static final int GROW_RATE = 1;
    public static final int GROW_THRESHOLD = 20;
    public static final int FIELD_INITIAL_RESOURCES = 10;
    public static final int MAX_RESOURCES = 50;
}
