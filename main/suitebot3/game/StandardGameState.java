package suitebot3.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import suitebot3.game.dto.GameStateDTO;
import suitebot3.game.dto.PlayerDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StandardGameState implements GameState
{
	private final GamePlan gamePlan;
	private final List<Player> players;
	private UpdatableFieldState[][] fields = null;
	private List<Point> positions;

	private final Map<Integer, PlayerState> playerStateMap = new HashMap<>();
	protected int roundsRemaining;
	protected int currentRound;

	public StandardGameState(GamePlan gamePlan, Player... players)
	{
		this(gamePlan, Arrays.asList(players));
	}

	public StandardGameState(GamePlan gamePlan, List<Player> players, boolean putAntsToHomeBases) {
		this(gamePlan, players);
		if (putAntsToHomeBases)
			this.putAntsToHomeBases();
	}

	public StandardGameState(GamePlan gamePlan, List<Player> players)
	{
		this.gamePlan = new StandardGamePlan(gamePlan);
		this.players = ImmutableList.copyOf(players);
		this.roundsRemaining = gamePlan.getMaxRounds();
		this.currentRound = 1;
		initPositions();

		assertPlayersUnique();
		assertPlayerIdsNotZero();

		if (players.size() != gamePlan.getHomeBasePositions().size())
		{
			throw new IllegalStateException(String.format("number of players (%d) is different from the number of starting positions (%d)",
					players.size(), gamePlan.getHomeBasePositions().size()));
		}

		for (Player player : players)
		{
			PlayerState playerState = new PlayerState(player);
			playerStateMap.put(player.id, playerState);
		}

		fields = new UpdatableFieldState[gamePlan.getWidth()][gamePlan.getHeight()];
		clearFields(getHomeBaseMap());
	}

	public StandardGameState(GameState gameState)
	{
		this.gamePlan = new StandardGamePlan(gameState.getGamePlan());
		this.players = ImmutableList.copyOf(gameState.getPlayers());
		this.roundsRemaining = gameState.getRoundsRemaining();
		this.currentRound = gameState.getCurrentRound();
		initPositions();

		assertPlayersUnique();
		assertPlayerIdsNotZero();

		for (Player player : players)
		{
			PlayerState playerState = new PlayerState(player);
			playerStateMap.put(player.id, playerState);
			playerState.setResources(gameState.getPlayerResources(playerState.getPlayer().id));
		}

		fields = new UpdatableFieldState[gamePlan.getWidth()][gamePlan.getHeight()];
		clearFields(getHomeBaseMap());

		// copy field resources
		gameState.allFields().forEach(fieldPos -> {
			UpdatableFieldState field = getFieldUpdatable(fieldPos.position);
			field.setResources(fieldPos.fieldState.getResourceCount());
		});

		// spawn
		for (Player otherPlayer : gameState.getPlayers())
		{
			for (Ant otherAnt : gameState.getAntsOfPlayer(otherPlayer.id))
				spawnAnt(otherAnt.getPosition(), getPlayerState(otherPlayer.id).getPlayer().id);
		}
	}

	public StandardGameState(GamePlan gamePlan, List<Player> players, int currentRound, int remainingRounds)
	{
		this(gamePlan, players);
		this.currentRound = currentRound;
		this.roundsRemaining = remainingRounds;
	}

	@Override
	public Stream<FieldPos> allFields()
	{
		return positions.stream()
				.map(position -> new FieldPos(position, fields[position.x][position.y]));
	}

	@Override
	public boolean isGameOver()
	{
		return roundsRemaining <= 0;
	}

	@Override
	public int getCurrentRound()
	{
		return currentRound;
	}

	@Override
	public int getRoundsRemaining()
	{
		return roundsRemaining;
	}

	@Override
	public List<Ant> getAntsOfPlayer(int playerId)
	{
		return getPlayerState(playerId).getAnts();
	}

	@Override
	public int getPlayerResources(int playerId)
	{
		return getPlayerState(playerId).getResources();
	}

	/**
	 * Preferably, use the following methods to manipulate player state:
	 *
	 *   Ant spawnAnt(Point position, int playerId);
	 *   void killAnt(Ant ant);
	 *   void moveAnt(Ant ant, Point targetPosition);
	 *
	 *   void setResourcesOnField(Point position, int resources);
	 *   void setPlayerResources(int playerId, int resources);
	 */
	protected PlayerState getPlayerState(int playerId)
	{
		checkThatPlayerIdIsCorrect(playerId);
		return playerStateMap.get(playerId);
	}

	private void checkThatPlayerIdIsCorrect(int playerId)
	{
		if (!playerStateMap.containsKey(playerId))
			throw new RuntimeException("No player with id " + playerId);
	}

	@Override
	public List<Player> getPlayers()
	{
		return players;
	}

	@Override
	public GamePlan getGamePlan()
	{
		return gamePlan;
	}

	@Override
	public FieldState getField(Point position)
	{
		gamePlan.checkIsOnPlan(position);
		return fields[position.x][position.y];
	}

	@Override
	public Ant spawnAnt(Point position, int playerId)
	{
		UpdatableAnt ant = getPlayerState(playerId).spawnNewAnt(position);
		getFieldUpdatable(position).setAnt(ant);
		return ant;
	}

	@Override
	public void killAnt(Ant ant)
	{
		getFieldUpdatable(ant.getPosition()).setAnt(null);
		getPlayerState(ant.getPlayer().id).killAnt(ant);
	}

	@Override
	public void moveAnt(Ant ant, Point targetPosition)
	{
		UpdatableAnt updatableAnt = (UpdatableAnt)ant;

		UpdatableFieldState sourceField = getFieldUpdatable(ant.getPosition());
		if (sourceField.getAnt() == ant)
			getFieldUpdatable(ant.getPosition()).setAnt(null);

		updatableAnt.setPosition(targetPosition);
		getFieldUpdatable(ant.getPosition()).setAnt(updatableAnt);
	}

	@Override
	public void setResourcesOnField(Point position, int resources)
	{
		getFieldUpdatable(position).setResources(resources);
	}

	@Override
	public void setPlayerResources(int playerId, int resources)
	{
		getPlayerState(playerId).setResources(resources);
	}

	@Override
	public GameStateDTO toDto()
	{
		GameStateDTO dto = new GameStateDTO();

		dto.currentRound = getCurrentRound();
		dto.remainingRounds = roundsRemaining;

		dto.gamePlanWidth = gamePlan.getWidth();
		dto.gamePlanHeight = gamePlan.getHeight();

		dto.players = new HashMap<>(players.size());
		for (Player player : players)
		{
			PlayerState playerState = getPlayerState(player.id);

			PlayerDTO playerDTO = new PlayerDTO();
			playerDTO.name = player.name;
			playerDTO.homeBaseLocation = gamePlan.getHomeBasePositions().get(player.id);
			playerDTO.antLocations = playerState.getAnts().stream().map(Ant::getPosition).collect(Collectors.toList());
			playerDTO.antLastMoves = playerState.getAnts().stream().map(Ant::getLastMove).collect(Collectors.toList());
			playerDTO.resources = playerState.getResources();

			dto.players.put(player.id, playerDTO);
		}

		dto.fieldResources = new ArrayList<>(gamePlan.getWidth());

		for (int x = 0; x < gamePlan.getWidth(); x++)
		{
			List<Integer> column = new ArrayList<>(gamePlan.getHeight());
			dto.fieldResources.add(column);

			for (int y = 0; y < gamePlan.getHeight(); y++)
				column.add(getField(new Point(x, y)).getResourceCount());
		}

		return dto;
	}

	protected Stream<UpdatableFieldPos> allFieldsUpdatable()
	{
		return positions.stream()
				.map(position -> new UpdatableFieldPos(position, fields[position.x][position.y]));
	}

	protected UpdatableFieldState getFieldUpdatable(Point position)
	{
		return fields[position.x][position.y];
	}

	private void putAntsToHomeBases()
	{
		allFieldsUpdatable()
				.filter(fieldPos -> fieldPos.fieldState.getBase() != null)
				.forEach(fieldPos -> {
					if (fieldPos.fieldState.getAnt() != null)
						throw new RuntimeException("Cannot put an ant to base " + fieldPos.position + ", there is an ant already.");

					spawnAnt(fieldPos.position, fieldPos.fieldState.getBase().id);
				});
	}

	private Map<Point, Player> getHomeBaseMap()
	{
		Map<Point, Player> homeBaseMap = new HashMap<>(players.size());

		for (Player player : players) {
			Point homeBasePosition = gamePlan.getHomeBasePositions().get(player.id);
			homeBaseMap.put(homeBasePosition, player);
		}

		return homeBaseMap;
	}

	private void clearFields(Map<Point, Player> homeBases)
	{
		for (int x = 0; x < gamePlan.getWidth(); x++)
		{
			for (int y = 0; y < gamePlan.getHeight(); y++)
			{
				Point position = new Point(x, y);
				Player baseOwner = homeBases.get(position);

				UpdatableFieldState field = new UpdatableFieldState(baseOwner);
				fields[x][y] = field;
			}
		}
	}

	private void initPositions()
	{
		positions = new ArrayList<>(gamePlan.getWidth() * gamePlan.getHeight());

		for (int x = 0; x < gamePlan.getWidth(); ++x)
		{
			for (int y = 0; y < gamePlan.getHeight(); ++y)
				positions.add(new Point(x, y));
		}
	}

	private void assertPlayersUnique()
	{
		Set<Player> uniquePlayers = ImmutableSet.copyOf(players);
		if (uniquePlayers.size() < players.size())
			throw new IllegalStateException("Players are not unique");
	}

	private void assertPlayerIdsNotZero()
	{
		for (Player player : players)
		{
			if (player.id == 0)
				throw new IllegalStateException("Player ID cannot be 0");
		}
	}

	protected static class UpdatableFieldPos
	{
		public final Point position;
		public final UpdatableFieldState fieldState;

		public UpdatableFieldPos(Point position, UpdatableFieldState fieldState)
		{
			this.position = position;
			this.fieldState = fieldState;
		}
	}
}
