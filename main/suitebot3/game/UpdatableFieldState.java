package suitebot3.game;

public class UpdatableFieldState implements FieldState
{
    private UpdatableAnt ant;
    private int resources;
    private final Player base;

    public UpdatableFieldState(Player base)
    {
        ant = null;
        this.base = base;
        resources = base != null ? 0 : GameConstants.FIELD_INITIAL_RESOURCES;
    }

    @Override
    public int getResourceCount()
    {
        return resources;
    }

    @Override
    public Ant getAnt()
    {
        return ant;
    }

    @Override
    public Player getBase()
    {
        return base;
    }

    public UpdatableAnt getAntUpdatable()
    {
        return ant;
    }

    public void setAnt(UpdatableAnt ant)
    {
        this.ant = ant;
    }

    public void setResources(int resources)
    {
        if (resources > GameConstants.MAX_RESOURCES)
            throw new IllegalStateException("Cannot set more resources than " + GameConstants.MAX_RESOURCES);
        this.resources = resources;
    }
}
