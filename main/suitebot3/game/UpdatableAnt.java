package suitebot3.game;

public interface UpdatableAnt extends Ant
{
    void setPosition(Point position);

    void setLastMove(Action lastMove);

    /** Kills the ant removes it from the player's list of ants. */
    void kill();
}
