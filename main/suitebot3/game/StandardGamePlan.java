package suitebot3.game;

import java.util.HashMap;
import java.util.Map;

public class StandardGamePlan implements GamePlan
{
	private int width;
	private int height;
	private Map<Integer, Point> startingPositions;
	private int maxRounds;

	public static final int DEFAULT_MAX_ROUNDS = 150;

	public StandardGamePlan(int width, int height, Map<Integer, Point> startingPositions)
	{
		this(width, height, startingPositions, DEFAULT_MAX_ROUNDS);
	}

	public StandardGamePlan(int width, int height, Map<Integer, Point> startingPositions, int maxRounds)
	{
		this.width = width;
		this.height = height;
		this.startingPositions = new HashMap<>(startingPositions);
		this.maxRounds = maxRounds;

		validateStartingPositions();
	}

	public StandardGamePlan(GamePlan gamePlan)
	{
		width = gamePlan.getWidth();
		height = gamePlan.getHeight();
		startingPositions = new HashMap<>(gamePlan.getHomeBasePositions());
		maxRounds = gamePlan.getMaxRounds();
	}

	private void validateStartingPositions()
	{
		for (Point position : startingPositions.values())
		{
			if (isOutsideOfPlan(position))
				throw new IllegalStateException("starting position is outside of the plan: " + position);
		}
	}

	@Override
	public void checkIsOnPlan(Point position)
	{
		if (isOutsideOfPlan(position))
			throw new PointOutsideOfGamePlan(position);
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public Map<Integer, Point> getHomeBasePositions()
	{
		return startingPositions;
	}

	@Override
	public int getMaxRounds()
	{
		return maxRounds;
	}

	private boolean isOutsideOfPlan(Point position)
	{
		return position.x < 0 || position.y < 0 || position.x >= width || position.y >= height;
	}
}
