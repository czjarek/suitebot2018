package suitebot3.game.util;

import suitebot3.game.GameConstants;

public class VisualisationUtil
{
    public static final String RESOURCES_CHARS = " .:-=+*#%@";
    public static final String BASE_CHARS = "ABCDEFGH";
    public static final String ANT_CHARS = "abcdefgh";

    public static boolean isResourceChar(char ch)
    {
        return RESOURCES_CHARS.indexOf(ch) > -1;
    }

    public static char getResourceChar(int resourceCount)
    {
        int resourceCharIndex = RESOURCES_CHARS.length() * resourceCount / (GameConstants.MAX_RESOURCES + 1);
        return RESOURCES_CHARS.charAt(resourceCharIndex);
    }

    public static int getResources(char ch)
    {
        int index = RESOURCES_CHARS.indexOf(ch);
        if (index < 0)
            throw new RuntimeException("'" + ch + "' is not a valid character for resources. Supported: " + RESOURCES_CHARS);

        return index * GameConstants.MAX_RESOURCES / RESOURCES_CHARS.length();
    }

    public static boolean isBaseChar(char ch)
    {
        return BASE_CHARS.indexOf(ch) > -1;
    }

    public static char getBaseChar(int playerId)
    {
        return BASE_CHARS.charAt(playerId - 1);
    }

    public static boolean isAntChar(char ch)
    {
        return ANT_CHARS.indexOf(ch) > -1;
    }

    public static char getAntChar(int playerId)
    {
        return ANT_CHARS.charAt(playerId - 1);
    }

    public static int getPlayerId(char ch)
    {
        int id = ANT_CHARS.indexOf(ch);
        if (id > -1)
            return id + 1;
        else
            return BASE_CHARS.indexOf(ch) + 1;
    }

}
