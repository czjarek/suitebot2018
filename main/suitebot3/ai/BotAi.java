package suitebot3.ai;

import suitebot3.game.GameState;
import suitebot3.game.Moves;

public interface BotAi
{
	/**
	 * @param gameState - the current state of the game
	 * @return the moves to play
	 */
	Moves makeMoves(GameState gameState);
}
