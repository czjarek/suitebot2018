package suitebot3.ai;

import com.google.common.collect.ImmutableList;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import suitebot3.game.Action;
import suitebot3.game.Ant;
import suitebot3.game.GameSetup;
import suitebot3.game.GameState;
import suitebot3.game.Moves;
import suitebot3.game.Point;
import suitebot3.util.GraphUtil;
import suitebot3.util.MoveUtil;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class SampleBotAi implements BotAi
{
	private int myId;
	private Random random = new Random();

	private static List<Action> MOVE_ACTIONS = ImmutableList.of(Action.UP, Action.LEFT, Action.DOWN, Action.RIGHT);

	public SampleBotAi(GameSetup gameSetup)
	{
		myId = gameSetup.aiPlayerId;
	}

	@Override
	public Moves makeMoves(GameState gameState)
	{
		Moves.Builder moves = new Moves.Builder(gameState, myId);

		if (shouldSpawn(gameState))
		{
			moves.spawnNewAnt();
		}

		Set<Point> forbidenPoints = new HashSet<>();

		for (Ant myAnt : gameState.getAntsOfPlayer(myId))
		{
			Point antPosition = myAnt.getPosition();

			double bestActionScore = 0;
			Action bestAction = null;

			for (Action action : MOVE_ACTIONS)
			{
				Point toPosition = MoveUtil.move(gameState.getGamePlan(), antPosition, action);
				if (forbidenPoints.contains(toPosition))
				{
					continue;
				}

				double actionScore = MoveUtil.getResourceScoreForPoint(toPosition, gameState, 4);

				if (bestActionScore <= actionScore)
				{
					bestAction = action;
					bestActionScore = actionScore;
				}
			}

			if (bestAction == null)
			{
				bestAction = Action.HOLD;
			}
			else if (bestActionScore == 0)
			{
				bestAction = ImmutableList.of(Action.UP, Action.LEFT).get(random.nextInt(2));
			}

			if (shouldExplode(gameState, myAnt))
			{
					bestAction = Action.EXPLODE;
			}

			Point forbiddenPoint = MoveUtil.move(gameState.getGamePlan(), antPosition, bestAction);
			forbidenPoints.add(forbiddenPoint);

			moves.moveAnt(myAnt, bestAction);
		}

		return moves.build();
	}

	private boolean shouldSpawn(@Nonnull GameState gameState)
	{
		long maxAnts = 18;

		Point home = gameState.getGamePlan().getHomeBasePositions().get(myId);

		double scoreAtHome = MoveUtil.getResourceScoreForPoint(home, gameState, 6);
		long myAntsCount = countMyAnts(gameState);
		int myResources = gameState.getPlayerResources(myId);

		long resourceMinimum = 50 + myAntsCount * 10;

		if (gameState.getAntsOfPlayer(1).size() < 2
				&& scoreAtHome == 0 && gameState.getCurrentRound() > 77)
		{
			return false;
		}


		if (myAntsCount < 2 && myResources > 3000)
		{
			return true;

		}

		if (myAntsCount < 5 && myResources > 1000 && scoreAtHome > 0)
		{
			return true;
		}

		if (scoreAtHome > 6 && myResources > resourceMinimum && myAntsCount < maxAnts)
		{
			return true;
		}

		return false;
	}

	private boolean shouldExplode(@Nonnull GameState gameState, @Nonnull Ant myAnt)
	{
		long minAnts = 2;

		Point antPosition = myAnt.getPosition();
		Point homePosition = gameState.getGamePlan().getHomeBasePositions().get(myId);

		// don't explode near home
		Graph<Point, DefaultEdge> graph = GraphUtil.gameStateToGraph(gameState.getGamePlan());
		Integer distanceFromHome = GraphUtil.distance(graph, homePosition, antPosition);
		if (distanceFromHome == null || distanceFromHome < 5) return false;

		double scoreAtPosition = MoveUtil.getResourceScoreForPoint(antPosition, gameState, 3);
		long myAntsCount = countMyAnts(gameState);

		if (scoreAtPosition < 0.2 && myAntsCount > minAnts)
		{
			return true;
		}

		return false;
	}

	private long countMyAnts(GameState gameState)
	{
		return gameState.getAntsOfPlayer(myId).size();
	}
}
