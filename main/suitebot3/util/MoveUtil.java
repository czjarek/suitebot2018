package suitebot3.util;

import com.google.common.collect.ImmutableSet;
import suitebot3.game.Action;
import suitebot3.game.GamePlan;
import suitebot3.game.GameState;
import suitebot3.game.Point;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


public final class MoveUtil
{
	@Nonnull
	public static Point move(@Nonnull GamePlan gamePlan, @Nonnull Point from, @Nonnull Action action)
	{
		int newX = from.x + action.dx;
		int newY = from.y + action.dy;

		if (newX < 0)
		{
			newX = gamePlan.getWidth() - 1;
		}

		if (newX > gamePlan.getWidth() - 1)
		{
			newX = 0;
		}

		if (newY < 0)
		{
			newY = gamePlan.getHeight() - 1;
		}

		if (newY > gamePlan.getHeight() - 1)
		{
			newY = 0;
		}

		return new Point(newX, newY);
	}

	@Nonnull
	public static Set<Point> neighbors(@Nonnull GamePlan gamePlan, @Nonnull Point point)
	{
		Set<Point> neighbors = new HashSet<>(4);
		neighbors.add(move(gamePlan, point, Action.UP));
		neighbors.add(move(gamePlan, point, Action.DOWN));
		neighbors.add(move(gamePlan, point, Action.LEFT));
		neighbors.add(move(gamePlan, point, Action.RIGHT));
		return neighbors;
	}

	@Nonnull
	public static Set<Point> neighbors(@Nonnull GamePlan gamePlan, @Nonnull Set<Point> points)
	{
		return points.stream()
				.flatMap(p -> neighbors(gamePlan, p).stream())
				.collect(Collectors.toSet());
	}


	// indexed from 0 no borderless
	@Nonnull
	public static Point moveUp(@Nonnull Point from, int width, int height)
	{
		int newY = from.y - 1;
		if (newY < 0)
		{
			newY += height;
		}
		return new Point(from.x, newY);
	}
	@Nonnull
	public static Point moveDown(@Nonnull Point from, int width, int height)
	{
		return new Point(from.x, (from.y + 1) % height);
	}
	@Nonnull
	public static Point moveLeft(@Nonnull Point from, int width, int height)
	{
		int newX = from.x - 1;
		if (newX < 0)
		{
			newX += width;
		}
		return new Point(newX, from.y);
	}
	@Nonnull
	public static Point moveRight(@Nonnull Point from, int width, int height)
	{
		return new Point((from.x + 1) % width, from.y);
	}

	@Nonnull
	public static Set<Point> neighbors(@Nonnull Point point, int width, int height)
	{
		Set<Point> neighbors = new HashSet<>(4);
		neighbors.add(moveUp(point, width, height));
		neighbors.add(moveDown(point, width, height));
		neighbors.add(moveLeft(point, width, height));
		neighbors.add(moveRight(point, width, height));
		return neighbors;
	}

	public static double getResourceScoreForPoint(@Nonnull Point point, @Nonnull GameState gameState, int deep)
	{
		if (deep == 0)
		{
			return gameState.getField(point).getResourceCount();
		}

		Set<Point> prevNeighbors = ImmutableSet.of(point);

		double resultScore = gameState.getField(point).getResourceCount();

		for (int i = 1; i <= deep; i++)
		{
			Set<Point> neighbors = MoveUtil.neighbors(gameState.getGamePlan(), prevNeighbors);
			double currentDeepScore = 0;
			for (Point neighbor : neighbors)
			{
				currentDeepScore += gameState.getField(neighbor).getResourceCount();
			}

			resultScore += (1/Math.pow(2, i))*currentDeepScore;

			prevNeighbors = neighbors;
		}

		return resultScore;
	}


	private MoveUtil()
	{
	}
}
