package suitebot3.util;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.alg.util.NeighborCache;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.Pseudograph;
import suitebot3.game.GamePlan;
import suitebot3.game.Point;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

public final class GraphUtil
{
    @Nonnull
    public static Graph<Point, DefaultEdge> gameStateToGraph(GamePlan gameplan)
    {
        Graph<Point, DefaultEdge> graph = new Pseudograph<>(DefaultEdge.class);

        // add vertices
        for (int x = 0; x < gameplan.getWidth(); x++)
        {
            for (int y = 0; y < gameplan.getHeight(); y++)
            {
                Point vertex = new Point(x, y);
                graph.addVertex(vertex);
            }
        }

        // add <-> edges between empty fields
        for (int x = 0; x < gameplan.getWidth(); x++)
        {
            for (int y = 0; y < gameplan.getHeight(); y++)
            {
                Point vertexFrom = new Point(x, y);
                for (Point vertexTo : MoveUtil.neighbors(vertexFrom, gameplan.getWidth(), gameplan.getHeight()))
                {
                    graph.addEdge(vertexFrom, vertexTo);
                }
            }
        }

        return graph;
    }

    @Nonnull
    public static Set<Point> neighborsOf(@Nonnull Graph<Point, DefaultEdge> graph, @Nonnull Point vertex)
    {
        NeighborCache<Point, DefaultEdge> neighborCache = new NeighborCache<>(graph);

        return neighborCache.neighborsOf(vertex);
    }

    @Nullable
    public static Integer distance(@Nonnull Graph<Point, DefaultEdge> graph, @Nonnull Point fromVertex, @Nonnull Point toVertex)
    {
       GraphPath<Point, DefaultEdge> shortestPath = DijkstraShortestPath.findPathBetween(graph, fromVertex, toVertex);
       return shortestPath == null ? null : shortestPath.getLength();
    }

    private GraphUtil()
    {
    }
}
