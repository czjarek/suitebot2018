package suitebot3.game;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PlayerStateTest
{

	private Player player;
	private PlayerState playerState;

	@Before
	public void setUp()
	{
		player = new Player(1, "1");
		playerState = new PlayerState(player);
		playerState.spawnNewAnt(new Point(1, 2));
	}

	@Test
	public void testGetAnt() throws Exception
	{
		assertThat(playerState.getAnt(0).getPosition(), equalTo(new Point(1, 2)));
	}

	@Test
	public void testGetAntPositions() throws Exception
	{
		assertThat(playerState.getAnts().get(0).getPosition(), equalTo(new Point(1, 2)));
	}

	@Test
	public void testGetAnts() throws Exception
	{
		assertThat(playerState.getAnts().size(), is(1));
	}

	@Test
	public void testKillAnt() throws Exception
	{
		playerState.killAnt(playerState.getAnt(0));
		assertThat(playerState.getAnts(), equalTo(Arrays.asList()));
	}

	@Test
	public void testKillAnt2() throws Exception
	{
		playerState.getAnt(0).kill();
		assertThat(playerState.getAnts(), equalTo(Arrays.asList()));
	}

	@Test
	public void testGetPlayer() throws Exception
	{
		assertThat(playerState.getPlayer(), is(player));
		assertThat(playerState.getAnt(0).getPlayer(), is(player));
	}

	@Test
	public void testSetPosition() throws Exception
	{
		playerState.getAnt(0).setPosition(new Point(5, 10));
		assertThat(playerState.getAnt(0).getPosition(), is(new Point(5, 10)));
	}
}