package suitebot3.game;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class MovesTest
{
	@Test
	public void step1HasTheValueSetInTheConstructor() throws Exception
	{
		Action action = Action.DOWN;
		Moves moves = new Moves(action);

		assertThat(moves.getAction(0), is(action));
	}

	@Test
	public void sameMovesShouldEqual() throws Exception
	{
		assertThat(new Moves(Action.RIGHT), equalTo(new Moves(Action.RIGHT)));
		assertThat(new Moves(), equalTo(new Moves()));
		assertThat(new Moves(false, Action.RIGHT, Action.UP),
		   equalTo(new Moves(false, Action.RIGHT, Action.UP)));
	}

	@Test
	public void differentMovesShouldNotEqual() throws Exception
	{
		assertThat(new Moves(Action.RIGHT), not(equalTo(new Moves(Action.LEFT))));
		assertThat(new Moves(null), not(equalTo(new Moves(Action.LEFT))));
		assertThat(new Moves(false, Action.RIGHT, Action.UP), not(equalTo(new Moves(Action.RIGHT))));
		assertThat(new Moves(true, Action.RIGHT), not(equalTo(new Moves(Action.LEFT))));
	}

	@Test
	public void holdDoesNotMove() throws Exception
	{
		assertThat(Action.HOLD.asString, equalTo("H"));
		assertThat(Action.HOLD.dx, equalTo(0));
		assertThat(Action.HOLD.dy, equalTo(0));
	}

	@Test
	public void explodeDoesNotMove() throws Exception
	{
		assertThat(Action.EXPLODE.asString, equalTo("E"));
		assertThat(Action.EXPLODE.dx, equalTo(0));
		assertThat(Action.EXPLODE.dy, equalTo(0));
	}

	@Test
	public void notMovingSerializedProperly() throws Exception
	{
		final StandardGameState gameState = GameStateCreator.fromString("A");

		String serialized =
				new Moves.Builder(gameState, 1)
				.spawnNewAnt()
				.build().serialize();

		assertThat(serialized, is("HS"));
	}

	@Test
	public void explodeSerializedProperly() throws Exception
	{
		final StandardGameState gameState = GameStateCreator.fromString("Aaa");

		List<Ant> ants = gameState.getAntsOfPlayer(1);

		String serialized =
				new Moves.Builder(gameState, 1)
						.moveAnt(ants.get(0), Action.UP)
						.moveAnt(ants.get(1), Action.HOLD)
						.moveAnt(ants.get(2), Action.EXPLODE)
						.spawnNewAnt()
						.build().serialize();

		assertThat(serialized, is("UHES"));
	}
}