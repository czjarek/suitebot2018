package suitebot3.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import suitebot3.game.dto.GameStateDTO;
import suitebot3.game.util.VisualisationUtil;

import java.util.Arrays;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class StandardGameStateTest
{
	public static final Player PLAYER1 = new Player(1, "Player 1");
	public static final Player PLAYER2 = new Player(2, "Player 2");

	private static final Point PLAYER1_START_12_X_12 = new Point(11, 5);
	private static final Point PLAYER2_START_12_X_12 = new Point(11, 3);

	private static final Map<Integer, Point> STARTING_POSITIONS_12_X_12 = ImmutableMap.of(1, PLAYER1_START_12_X_12, 2, PLAYER2_START_12_X_12);

	public static final GamePlan GAME_PLAN_12_X_12 = new StandardGamePlan(12, 12, STARTING_POSITIONS_12_X_12);
	private GameState gameState;

	@Before
	public void setUp() throws Exception
	{
		gameState = new StandardGameState(GAME_PLAN_12_X_12, Arrays.asList(PLAYER1, PLAYER2), true);
	}

	@Test(expected = IllegalStateException.class)
	public void mismatchBetweenStartingPositionsCountAndPlayerCountShouldThrowException() throws Exception
	{
		new StandardGameState(GAME_PLAN_12_X_12, PLAYER1);
	}

	@Test
	public void testGetPlayers() throws Exception
	{
		assertThat(ImmutableList.copyOf(gameState.getPlayers()), equalTo(ImmutableList.of(PLAYER1, PLAYER2)));
	}

	@Test
	public void isAntAndBaseOnEmptyField_returnFalse() throws Exception
	{
		FieldState field00 = gameState.getField(new Point(0, 0));
		assertThat(field00.getAnt(), is(nullValue()));
		assertThat(field00.getBase(), is(nullValue()));
	}

	@Test
	public void isAntOnFieldOnOccupiedField_returnsTrue() throws Exception
	{
		assertThat(gameState.getField(PLAYER1_START_12_X_12).getAnt(), is(not(nullValue())));
	}

	@Test(expected = GamePlan.PointOutsideOfGamePlan.class)
	public void getFieldOutsidePlan_shouldThrow() throws Exception
	{
		gameState.getField(new Point(-1, 0));
	}

	@Test
	public void playerStatesShouldBeInitializedFromGamePlan() throws Exception
	{
		assertThat(gameState.getAntsOfPlayer(PLAYER1.id).get(0).getPosition(), equalTo(PLAYER1_START_12_X_12));
		assertThat(gameState.getAntsOfPlayer(PLAYER2.id).get(0).getPosition(), equalTo(PLAYER2_START_12_X_12));
	}

	@Test(expected = IllegalStateException.class)
	public void duplicatePlayerIdShouldThrowException() throws Exception
	{
		new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, new Player(PLAYER1.id, "Duplicate ID player"));
	}

	@Test
	public void testDtoGamePlan() throws Exception
	{
		GameStateDTO dto = GameStateCreator.fromString("Aa#\n   ").toDto();
		assertThat(dto.gamePlanWidth, is(3));
		assertThat(dto.gamePlanHeight, is(2));
	}

	@Test
	public void testDtoPlayerHomeBase() throws Exception
	{
		GameStateDTO dto = GameStateCreator.fromString("A  \n B ").toDto();
		assertThat(dto.players.size(), is(2));
		assertThat(dto.players.get(1).homeBaseLocation, is(new Point(0, 0)));
		assertThat(dto.players.get(2).homeBaseLocation, is(new Point(1, 1)));
	}

	@Test
	public void testDtoFieldResources() throws Exception
	{
		GameStateDTO dto = GameStateCreator.fromString(" # \n  #").toDto();
		assertThat(dto.fieldResources.get(0).get(0), is(VisualisationUtil.getResources(' ')));
		assertThat(dto.fieldResources.get(1).get(0), is(VisualisationUtil.getResources('#')));
		assertThat(dto.fieldResources.get(2).get(1), is(VisualisationUtil.getResources('#')));
	}

	@Test
	public void testSpawnAnt() throws Exception
	{
		final Point position = new Point(1, 0);
		StandardGameState gameState = GameStateCreator.fromString("A ");
		Ant ant = gameState.spawnAnt(position, 1);

		assertThat(ant.getPosition(), is(position));
		assertThat(ant.getPlayer().id, is(1));
		assertThat(ant.getLastMove(), is(nullValue()));

		assertThat(gameState.getField(position).getAnt(), is(ant));
	}

	@Test
	public void testMoreWaysHowToGetAnt() throws Exception
	{
		StandardGameState gameState = GameStateCreator.fromString("A");
		Ant antFromPlayer = gameState.getPlayerState(1).getAnt(0);
		Ant antFromField = gameState.getField(new Point(0, 0)).getAnt();

		assertThat(antFromPlayer, is(antFromField));
		assertThat(antFromPlayer.getPosition(), is(new Point(0, 0)));
		assertThat(antFromField.getPlayer().id, is(1));
	}

	@Test
	public void testKillingAnt() throws Exception
	{
		StandardGameState gameState = GameStateCreator.fromString("A");
		gameState.killAnt(gameState.getAntsOfPlayer(1).get(0));

		assertThat(gameState.getPlayerState(1).getAnts().size(), is(0));
		assertThat(gameState.getField(new Point(0, 0)).getAnt(), is(nullValue()));
	}

	@Test
	public void testMovingAnt() throws Exception
	{
		StandardGameState gameState = GameStateCreator.fromString("A ");
		Ant ant = gameState.getField(new Point(0, 0)).getAnt();
		gameState.moveAnt(ant, new Point(1, 0));

		assertThat(ant.getPosition(), is(new Point(1, 0)));
		assertThat(gameState.getField(new Point(0, 0)).getAnt(), is(nullValue()));
		assertThat(gameState.getField(new Point(1, 0)).getAnt(), is(ant));
	}

	@Test
	public void testSetResources() throws Exception
	{
		StandardGameState gameState = GameStateCreator.fromString("A");
		gameState.setPlayerResources(1, 500);

		assertThat(gameState.getPlayerResources(1), is(500));
	}

	@Test
	public void testSetResourcesOnField() throws Exception
	{
		StandardGameState gameState = GameStateCreator.fromString("A.");

		gameState.setResourcesOnField(new Point(1, 0), 5);
		assertThat(gameState.getField(new Point(1, 0)).getResourceCount(), is(5));
	}

	@Test(expected = IllegalStateException.class)
	public void settingMoreResourcesThanAllowed_throws() throws Exception
	{
		StandardGameState gameState = GameStateCreator.fromString("A.");

		gameState.setResourcesOnField(new Point(1, 0), 500000);
		assertThat(gameState.getField(new Point(1, 0)).getResourceCount(), is(GameConstants.MAX_RESOURCES));
	}
}
