package suitebot3.ai;

import org.junit.Ignore;
import org.junit.Test;
import suitebot3.game.Ant;
import suitebot3.game.Action;
import suitebot3.game.GameSetup;
import suitebot3.game.GameState;
import suitebot3.game.GameStateCreator;
import suitebot3.game.Moves;
import suitebot3.game.Point;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;


public class SampleBotAiTest
{
    private static final int AI_PLAYER_ID = 1;
    private GameState gameState;
    private Moves moves;

    @Ignore
    @Test
    public void whenHas4Ants_spawns() throws Exception
    {
        assertThat(move("Aaaa").getSpawnNewAnt(), is(true));
    }

    @Test
    public void whenHas5Ants_doesNotSpawn() throws Exception
    {
        assertThat(move("Aaaaa").getSpawnNewAnt(), is(false));
    }

    @Ignore
    @Test
    public void testMovesRandomly() throws Exception
    {
        Map<Action, Integer> actions = new HashMap<>(1000);

        for (int i = 0; i < 1000; ++i)
        {
            move("   \n" +
                    " A \n" +
                    "   ");

            Action action = getAntMove(new Point(1, 1));
            actions.compute(action, (key, count) -> count == null ? 1 : count + 1);
        }

        Action moveActions[] = {Action.UP, Action.DOWN, Action.LEFT, Action.RIGHT};

        assertThat(actions.size(), is(4));
        for (Action moveAction : moveActions)
            assertThat(actions.get(moveAction), greaterThan(1000 / (1 + moveActions.length)));

    }

    private Action getAntMove(Point point)
    {
        Ant ant = gameState.getField(point).getAnt();
        if (ant == null)
            throw new NoSuchElementException("There is no ant at position " + point);

        int antIndex = gameState.getAntsOfPlayer(AI_PLAYER_ID).indexOf(ant);
        if (antIndex < 0)
            throw new IllegalStateException("The ant doesn't belong to SampleBot");

        if (antIndex >= moves.getDirectionSize())
            return null;
        else
            return moves.getAction(antIndex);
    }

    private Moves move(String gameStateString)
    {
        gameState = GameStateCreator.fromString(gameStateString);
        SampleBotAi ai = new SampleBotAi(GameSetup.fromGameState(AI_PLAYER_ID, gameState));
        moves = ai.makeMoves(gameState);
        return moves;
    }
}