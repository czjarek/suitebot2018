package suitebot3.util;

import org.jgrapht.Graph;
import org.junit.Test;
import suitebot3.game.GameState;
import suitebot3.game.GameStateCreator;
import suitebot3.game.Point;

import javax.annotation.Nonnull;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GraphUtilTest
{
	@Test
	public void test()
	{
		Graph graph = GraphUtil.gameStateToGraph(createTestGameState().getGamePlan());

		assertThat(
				GraphUtil.neighborsOf(graph, new Point(0, 0)).size(),
				is(4));
	}

	@Test
	public void testDistance()
	{
		Graph graph = GraphUtil.gameStateToGraph(createTestGameState().getGamePlan());

		assertThat(
				GraphUtil.distance(graph, new Point(0, 0), new Point(2, 1)),
				is(3));
	}

	@Nonnull
	private GameState createTestGameState() {
		return GameStateCreator.fromString("A   \n"+"  B \n"+" C  ");
	}
}
