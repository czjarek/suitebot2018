package suitebot3.util;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import suitebot3.game.Action;
import suitebot3.game.GamePlan;
import suitebot3.game.GameState;
import suitebot3.game.GameStateCreator;
import suitebot3.game.Point;
import suitebot3.game.StandardGamePlan;

import javax.annotation.Nonnull;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MoveUtilTest
{
    @Test
    public void testMove_basicMoves_nowrap()
    {
        final GamePlan gamePlan = new StandardGamePlan(5, 5, Collections.emptyMap());

        final Point from = new Point(2, 2);
        assertThat(
                MoveUtil.move(gamePlan, from, Action.UP),
                is(new Point(from.x, from.y - 1)));

        assertThat(
                MoveUtil.move(gamePlan, from, Action.DOWN),
                is(new Point(from.x, from.y + 1)));

        assertThat(
                MoveUtil.move(gamePlan, from, Action.LEFT),
                is(new Point(from.x - 1, from.y)));

        assertThat(
                MoveUtil.move(gamePlan, from, Action.RIGHT),
                is(new Point(from.x + 1, from.y)));
    }

    @Test
    public void testMove_wrap()
    {
        final GamePlan gamePlan = new StandardGamePlan(5, 5, Collections.emptyMap());

        final Point upLeftCorner = new Point(0, 0);
        final Point bottomRightCorner = new Point(gamePlan.getWidth() - 1, gamePlan.getHeight() - 1);

        assertThat(
                MoveUtil.move(gamePlan, upLeftCorner, Action.UP),
                is(new Point(upLeftCorner.x, gamePlan.getHeight() - 1)));

        assertThat(
                MoveUtil.move(gamePlan, bottomRightCorner, Action.DOWN),
                is(new Point(bottomRightCorner.x, 0)));

        assertThat(
                MoveUtil.move(gamePlan, upLeftCorner, Action.LEFT),
                is(new Point(gamePlan.getWidth() - 1, upLeftCorner.y)));

        assertThat(
                MoveUtil.move(gamePlan, bottomRightCorner, Action.RIGHT),
                is(new Point(0, bottomRightCorner.y)));
    }

    @Test
    public void testGetFieldScore_maxResourceFieldAsBase()
    {
        GameState gameState = createTestGameState();

        Point maxResourcePoint = new Point(1, 1);
        double maxResourceValue = 20;

        gameState.setResourcesOnField(maxResourcePoint, (int)maxResourceValue);

        double resourceScoreForMaxResourcePoint = MoveUtil.getResourceScoreForPoint(maxResourcePoint, gameState, 1);
        MatcherAssert.assertThat(resourceScoreForMaxResourcePoint, is(equalTo(maxResourceValue)));
    }

    @Test
    public void testGetFieldScore_maxResourceFieldAsNeighbor()
    {
        GameState gameState = createTestGameState();

        Point maxResourcePoint = new Point(1, 1);
        double maxResourceValue = 20;

        gameState.setResourcesOnField(maxResourcePoint, (int)maxResourceValue);


        double resourceScoreForAnotherPoint = MoveUtil.getResourceScoreForPoint(MoveUtil.moveLeft(maxResourcePoint, gameState.getGamePlan().getWidth(), gameState.getGamePlan().getHeight()), gameState, 1);
        MatcherAssert.assertThat(resourceScoreForAnotherPoint, is(equalTo((double)10)));
    }

    @Test
    public void testGetFieldScore_allFieldResourcesZero()
    {
        GameState gameState = createTestGameState();

        Point basePoint = new Point(0, 2);

        double resourceScoreForBasePoint = MoveUtil.getResourceScoreForPoint(basePoint, gameState, 1);
        MatcherAssert.assertThat(resourceScoreForBasePoint, is(equalTo(0d)));
    }

    @Test
    public void testGetFieldScore_deepEqualToTwo()
    {
        GameState gameState = createTestGameState();

        Point maxResourcePoint = new Point(1, 1);
        double maxResourceValue = 20;

        Point anotherPointWithResources = MoveUtil.moveUp(maxResourcePoint, gameState.getGamePlan().getWidth(), gameState.getGamePlan().getHeight());
        double anotherPointResourcesValue = 10;

        gameState.setResourcesOnField(maxResourcePoint, (int)maxResourceValue);
        gameState.setResourcesOnField(anotherPointWithResources, (int)anotherPointResourcesValue);


        double resourceScoreForAnotherPoint = MoveUtil.getResourceScoreForPoint(MoveUtil.moveLeft(maxResourcePoint, gameState.getGamePlan().getWidth(), gameState.getGamePlan().getHeight()), gameState, 2);
        MatcherAssert.assertThat(resourceScoreForAnotherPoint, is(equalTo(12.5d)));
    }

    @Nonnull
    private GameState createTestGameState() {
        return GameStateCreator.fromString("A   \n"+"  B \n"+" C  ");
    }
}
